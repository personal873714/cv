import type { Metadata } from 'next'
import Image from 'next/image'
import Profile from '@/components/Profile'

export const metadata: Metadata = {
  title: 'Paul Page',
  description: 'Welcome to Next.js',
}
export default function Home() {
  return (
    <div className={'sm:flex justify-center gap-10 items-center'}>
      <Profile />
      <div className={'ml-auto mr-auto relative rounded-full h-96 w-96 overflow-hidden'}>
        <Image
          src={'/avatar.png'}
          alt={'avatar'}
          fill
        />
      </div>
    </div>
  )
}
