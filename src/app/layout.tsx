import { Inter } from 'next/font/google'
import Menu from '@/components/Menu'
import ReduxProvider from '@/redux/provider'
import './globals.css'

export const metadata = {
  title: 'Create Next App',
  description: 'Generated by create next app',
}
const inter = Inter({
  weight: '400',
  subsets: ['latin']
})

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={`${inter.className} h-full`}>
        <ReduxProvider>
          <div className={'mt-20 ml-2'}>
            {children}
          </div>
          <Header>
            <Menu />
          </Header>
          <Footer />
        </ReduxProvider>
      </body>
    </html>
  )
}

function Header ({ children } : { children: React.ReactNode}) {
  return (
    <div className={'fixed top-0 inset-x-0 p-1 flex bg-white'}>
      {children}
    </div>
  )
}

function Footer () {
  return (
    <div className={'fixed bottom-0 inset-x-0'}>© 2023 paullai.dev. All rights reserved.</div>
  )
}
